import warnings

warnings.filterwarnings("ignore")

import numpy as np

np.random.seed(1234)
import os
import pandas as pd
import pickle
import itertools
import matplotlib as plt
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.preprocessing import StandardScaler

from sklearn.svm import LinearSVC as SVC
from sklearn import clone
from sklearn.metrics import confusion_matrix

from utils import bootstrap_perf, DAP, compute_metrics
from config import TEST_RATIO, CVALUES
from models import *
# %% load data and define variables

DATASET = 'data'

features = features_data2['m2']



target = 'LS2'

# %%
SAVEDIR = f'../out/data2'
DATAFILE = f'../data/data2/{DATASET}.csv'

data = pd.read_csv(DATAFILE, index_col=0)

data.dropna(axis=0, inplace=True)

results_all = {}

# %% split train test
data_features = data[features]
data_target = data[target]

ss = StratifiedShuffleSplit(1, test_size=TEST_RATIO)
idx_train, idx_test = next(ss.split(data_features, data_target))

data_train = data.iloc[idx_train]
data_test = data.iloc[idx_test]

# %% obtain features and target
features_train = data_train[features]
target_train = data_train[target]

features_test = data_test[features]
target_test = data_test[target]

# %% normalize features
scaler_features = StandardScaler()
scaler_features.fit(features_train)

features_train = pd.DataFrame(scaler_features.transform(features_train),
                              index=features_train.index,
                              columns=features_train.columns)

features_test = pd.DataFrame(scaler_features.transform(features_test),
                             index=features_test.index,
                             columns=features_test.columns)

# %% set model and params
model = SVC(class_weight='balanced')

model_params = {'C': CVALUES}

keys, values = zip(*model_params.items())
param_grid = [dict(zip(keys, v)) for v in itertools.product(*values)]

# %% run the dap
results_dap = DAP(features_train, target_train,
                  model, param_grid,
                  importance_mode='permutation')

results_all['dap'] = results_dap

# %% get best p
CI_valid_ = []

# extract info for each feat step
for N in results_dap.keys():
    # best p
    idx_best_p = results_dap[N]['best_p_idx']
    p = results_dap[N]['best_p']
    CI_valid_.append(results_dap[N]['best_p_CI'])

# create nparray of CIs
CI_valid_ = np.array(CI_valid_)

# %% get best N (for best p)
idx_best_N = np.argmax(CI_valid_[:, 0])
best_N = list(results_dap.keys())[idx_best_N]

# get C for best N
best_p = results_dap[best_N]['best_p']

# get ranking for best N
features_ranked = results_dap[best_N]['best_p_ranking']

print(f'Best N features: {best_N} - Best p: {best_p}')

results_all['selection'] = {'best_N': best_N,
                            'best_p': best_p,
                            'best_p_idx': results_dap[best_N]['best_p_idx'],
                            'features_ranked': features_ranked}

# %% train best model and predict on train and test
model_best = clone(model)
model_best.set_params(C=best_p['C'])

features_train_best = features_train.reindex(columns=features_ranked)
model_best.fit(features_train_best.values, target_train.values)
pred_train = model_best.predict(features_train_best.values)

features_test_best = features_test.reindex(columns=features_ranked)
pred_test = np.round(model_best.predict(features_test_best.values))

results_all['best_model'] = {'pred_train': pred_train,
                             'true_train': target_train.values,
                             'pred_test': pred_test,
                             'true_test': target_test.values,
                             'model': model_best}

# %% save results
# from datetime import datetime

# now = datetime.now().strftime('%Y%m%d-%H%M')

OUTDIR = os.path.join(SAVEDIR, "m2")
os.makedirs(OUTDIR, exist_ok=True)


outfile = f'{OUTDIR}/results_dap'

with open(outfile, 'wb') as f:
    pickle.dump(results_all, f)

print(outfile)

# %% get bootstrap performance indicators on train and test
CI_perf_train = bootstrap_perf(results_all['best_model']['true_train'],
                               results_all['best_model']['pred_train'],
                               compute_metrics)

CI_perf_test = bootstrap_perf(results_all['best_model']['true_test'],
                              results_all['best_model']['pred_test'],
                              compute_metrics)

CI_perf_train = pd.DataFrame(CI_perf_train, index=['median', '5%', '95%'], columns=['MCC', 'precision', 'recall'])
CI_perf_test = pd.DataFrame(CI_perf_test, index=['median', '5%', '95%'], columns=['MCC', 'precision', 'recall'])

print(CI_perf_train.transpose())
print(CI_perf_test.transpose())

print(confusion_matrix(results_all['best_model']['true_train'],
                       results_all['best_model']['pred_train']))

print(confusion_matrix(results_all['best_model']['true_test'],
                       results_all['best_model']['pred_test']))

# CI_perf_train, CI_perf_train_mean = bootstrap_perf(target_train.values, pred_train, function=performance_metric,
#                                                    pred_value=np.mean(target_train.values))
# f, axes = plt.subplots(2, 1, sharex=True)
#
# f.set_size_inches(10, 10)
####
# plt.sca(axes[0])
# plt.plot(CI_train_[:, 0], 'b')
# plt.plot(CI_valid_[:, 0], 'r')
#
# plt.plot(CI_train_[:, 1], 'b--')
# plt.plot(CI_train_[:, 2], 'b--')
#
# plt.plot(CI_valid_[:, 1], 'r--')
# plt.plot(CI_valid_[:, 2], 'r--')
#
# plt.xticks(np.arange(CI_valid_.shape[0]), x_ticks_)
# plt.legend(['Train', 'Valid'])
#
#