import warnings
warnings.filterwarnings("ignore")
#@title
import numpy as np

import pandas as pd
from utils import bootstrap_CI, bootstrap_perf
import matplotlib.pyplot as plt
from sklearn.metrics import matthews_corrcoef as mcc
import pickle

#%% SETTINGS
DATASET = 'data2'
RESULTDIR = 'm2'

performance_metric = mcc

#%%
resultsfile = f'../../out/{DATASET}/{RESULTDIR}/results_dap'

#% load results
with open(resultsfile, 'rb') as f:
    results_container = pickle.load(f)

results_dap = results_container['dap']

best_N = results_container['selection']['best_N']
best_p = results_container['selection']['best_p']
best_p_idx = results_container['selection']['best_p_idx']

#%% extract CI for each feature step and rankings
x_ticks_ = []

CI_train_ = []
CI_valid_ = []

MAX_FEATURES_TO_CONSIDER = 12

N_FEAT = np.max(list(results_dap.keys()))
feature_ranks_N = pd.DataFrame(index = results_dap[N_FEAT]['best_p_ranking'][:MAX_FEATURES_TO_CONSIDER])


#to store the CI for the next plot
CI_DAP_train = None
CI_DAP_valid = None

# extract info for each feat step
for N in results_dap.keys():
    best_p_ = results_dap[N]['best_p']
    best_p_idx_ = results_dap[N]['best_p_idx']
    x_ticks_.append(f'{N}\n[{best_p_}]') #compose N and best C to be used as xlabels
    
    #CI
    ci_train = bootstrap_CI(results_dap[N][best_p_idx_]['performances_p'][:, 0])
    ci_valid = list(results_dap[N]['best_p_CI'])
    
    CI_train_.append(ci_train)
    CI_valid_.append(ci_valid)
    
    if N == best_N:
        CI_DAP_train = ci_train
        CI_DAP_valid = ci_valid
    
    #ranked features
    feature_ranked = results_dap[N]['best_p_ranking']
    
    #fill the table with the rankings
    ranks_N = []
    for f in feature_ranks_N.index:
        if f in feature_ranked:
            rank = MAX_FEATURES_TO_CONSIDER - np.where(feature_ranked.values == f)[0][0]
            
            #do not store ranks below the MAX_FEATURES_TO_CONSIDER
            #for these ranks store -1
            if rank < 1:
                ranks_N.append(-1)
            else:
                ranks_N.append(rank)
        else:
            ranks_N.append(np.nan)
    feature_ranks_N[N] = ranks_N

#%% PLOT performances of DAP across feature steps
#create nparray of CIs
CI_train_ = np.array(CI_train_)
CI_valid_ = np.array(CI_valid_)

f, axes = plt.subplots(2,1)

f.set_size_inches(15,10)

#####
plt.sca(axes[0])
plt.plot(CI_train_[:,0], 'b')
plt.plot(CI_valid_[:,0], 'r')

plt.plot(CI_train_[:,1], 'b--')
plt.plot(CI_train_[:,2], 'b--')

plt.plot(CI_valid_[:,1], 'r--')
plt.plot(CI_valid_[:,2], 'r--')

plt.grid()
plt.xticks(np.arange(CI_valid_.shape[0]), x_ticks_)
plt.legend(['Train', 'Valid'])
plt.ylabel('MCC')

#####
plt.sca(axes[1])
for f in feature_ranks_N.index:
    ranks_feature = feature_ranks_N.loc[f,:]
    plt.plot(ranks_feature.values, 'o-')

plt.hlines(0, 0, CI_valid_.shape[0]-1, color = 'k', linestyle='--')

plt.grid()
plt.xticks(np.arange(CI_valid_.shape[0]), x_ticks_);
plt.yticks(np.arange(1,MAX_FEATURES_TO_CONSIDER+1), feature_ranks_N.index[::-1]);

plt.savefig(f'../../out/{DATASET}/{RESULTDIR}/DAP.png')

#%% Summarize performance

#% compute performances of final model
results_performance = results_container['best_model']

CI_perf_train = bootstrap_perf(results_performance['true_train'], 
                               results_performance['pred_train'],
                               performance_metric)

CI_perf_test = bootstrap_perf(results_performance['true_test'], 
                              results_performance['pred_test'],
                              performance_metric)

#create dataframe with performances
perf_pd = pd.DataFrame({'DAP_train': CI_DAP_train,
                        'DAP_valid': CI_DAP_valid,
                        'Train': CI_perf_train,
                        'Test': CI_perf_test}, index = ['median', 'low', 'high'])

perf_pd.to_csv(f'../../out/{DATASET}/{RESULTDIR}/CI.csv')

#%%
#plot performances
for i in range(4):
    plt.bar(i, perf_pd.iloc[0,i])
    plt.plot([i,i], [perf_pd.iloc[1,i], perf_pd.iloc[2,i]], 'k')

plt.xticks(range(4), perf_pd.columns)
plt.savefig(f'../../out/{DATASET}/{RESULTDIR}/CI.png')
