import numpy as np
import sys

from sklearn.model_selection import KFold
from sklearn.inspection import permutation_importance
from sklearn import clone
from sklearn.metrics import matthews_corrcoef, precision_score, recall_score, make_scorer

def borda_sort(X, isranking=True):
    #returns the indices that would sort the features
    #according to the borda count 
    X_rank = np.apply_along_axis(np.argsort, 1, X)
    
    n_feat = X_rank.shape[1]
    counts = np.zeros(n_feat)    
    for i_row in range(X_rank.shape[0]):
        for i_col in range(n_feat):
            counts[X_rank[i_row, i_col]] = counts[X_rank[i_row, i_col]] + n_feat - i_col
    
    if isranking:
        return(np.argsort(counts)[::-1])
    else:
        return(np.argsort(counts))

def bootstrap_CI(x, K=0.25, N=1000):
    K = int(K*len(x))
    out = []
    for i in range(N):
        x_ = np.random.choice(x, K)
        out.append(np.mean(x_))
    return(np.percentile(out, 50), np.percentile(out, 5), np.percentile(out, 95))

def bootstrap_perf(y_true, y_pred, function, K=0.25, N=1000, pred_value = None):
    indices = np.arange(len(y_true))
    K = int(K*len(indices))
    
    out = []
    for i in range(N):
        idx_select = np.random.choice(indices, K)
        y_true_ = y_true[idx_select]
        y_pred_ = y_pred[idx_select]
        out.append(function(y_true_, y_pred_))
    
    out = np.array(out)
    return([np.percentile(out, 50, axis=0), 
            np.percentile(out, 5, axis=0), 
            np.percentile(out, 95, axis=0)])

def compute_metrics(y_true, y_pred):
    nclasses = len(np.unique(y_true))
    average = 'binary'
    if nclasses > 2:
        average = 'weighted'
            
    MCC = matthews_corrcoef(y_true, y_pred)
    # CM = confusion_matrix(y_true, y_pred)
    prec = precision_score(y_true, y_pred, average=average)
    rec = recall_score(y_true, y_pred, average=average)
    
    return(np.array([MCC, prec, rec]))

def DAP(features, target, 
        model, param_grid, 
        n_feat_step = None, CV_K=5, CV_N=10,
        importance_mode='model'):
    
    if n_feat_step is None:
        n_feat_step = features.shape[1]
    
    #create a vector with increasing number of features
    N_features = np.unique(np.linspace(1, features.shape[1], n_feat_step).astype(int))
    #ensure last item includes all features
    N_features[-1] = features.shape[1]
    #reverse, so we start from all features
    N_features = N_features[::-1]
    
    features_prev_step = features
    results_dict = {}

    #for each feature step
    for N in N_features:
        features_step = features_prev_step.iloc[:, :N]
        target_step = target.copy()

        #% get numpy arrays and normalize
        X = features_step.values
        y = target_step.values

        #% create the K-fold splitter
        skf = KFold(n_splits=CV_K, shuffle=True)

        #for each param set
        results_N = {}

        for i_p, p in enumerate(param_grid):
            #% set model template
            model_template = clone(model)
            model_template.set_params(C = p['C'])
            
            #%
            performances_p = []    
            importances_p = []

            #EXTERNAL CYCLE (CV_N times)
            for i_n in range(CV_N): #repeat CV_N times
                sys.stdout.write('\r {}, {}'.format(N, p))

                for idx_tr, idx_vl in skf.split(X, y): #for each fold
                    #create train and validation sets using k-fold splitting indexes
                    X_tr, y_tr = X[idx_tr, :], y[idx_tr]
                    X_vl, y_vl = X[idx_vl, :], y[idx_vl]

                    #initialize the model
                    model_ = clone(model_template)

                    #train
                    model_.fit(X_tr, y_tr)

                    #extract and save IMPORTANCE
                    if importance_mode == 'permutation':
                        importance = permutation_importance(model_,
                                                            X_vl, y_vl, 
                                                            n_repeats=20, 
                                                            n_jobs=-1,
                                                            scoring=make_scorer(matthews_corrcoef))['importances_mean']
                    else:
                        importance = abs(model_.coef_[0,:])
                        
                    importances_p.append(importance)

                    #simple prediction
                    y_tr_pred = model_.predict(X_tr)
                    y_vl_pred = model_.predict(X_vl)

                    #get performance
                    perf_tr = matthews_corrcoef(y_tr, y_tr_pred);
                    perf_vl = matthews_corrcoef(y_vl, y_vl_pred);

                    performances_p.append([perf_tr, perf_vl])
                    #END FOLD
                #END ITERATION


            #store peformances and importances for this C
            performances_p = np.array(performances_p)
            importances_p = np.array(importances_p)
            results_p = {'performances_p': performances_p,
                         'importances_p':  importances_p}
            
            results_N[i_p] = results_p
            #END p

        results_dict[N] = results_N

        #get the best performance across all ps based on performance_metric on valid

        #  get CI
        #CI_train = []
        CI_valid = []
        for i_p in range(len(param_grid)):
            #performances_CI_train = bootstrap_CI(results_feat_step_C['performances_10x5'][:, 0])
            performances_CI_valid = bootstrap_CI(results_N[i_p]['performances_p'][:, 1])
            #CI_train.append(performances_CI_train)
            CI_valid.append(performances_CI_valid)
        CI_valid = np.array(CI_valid)

        #  get p with best perform
        idx_best_p = np.argmax(CI_valid[:,0])
        best_p = param_grid[idx_best_p]

        # save the results
        results_dict[N]['best_p_idx'] = idx_best_p
        results_dict[N]['best_p'] = best_p
        results_dict[N]['best_p_CI'] = CI_valid[idx_best_p]


        #===========================
        # now rank features based on ranking on best C

        #get importances best p
        importances_best = results_N[idx_best_p]['importances_p']

        ranking_borda = borda_sort(importances_best, isranking=False)
        features_ranked = features_step.columns[ranking_borda]

        results_dict[N]['best_p_ranking'] = features_ranked

        features_prev_step = features_step[features_ranked]
        #END FEATURE STEP
        
    return(results_dict)