# features_data1 rappresenta diverse combinazioni di item (m2...m*) che si possono ottenere sul dataset risultato
# dal processing del dataset data1 basato sul questionario wm.

# data1 = {'wm': ['WB6A','WB16','MT9','MT11','CM3','CM8','CP2','UN7','UN14N','VT9','MA1','AF10','AF11','SB2U', 'LS2','WB4','AF6','AF8','AF9','SB1','WB18','MT1',]}

features_data1 = {
                    'm2':['MA1','AF10','VT9','UN14','MT9','MT11','UN7','CM3','CM8','WB6A','WB16','CP2'],
                    'm2':['WB16','Mt9','MT11','CM3','CM8','CP2','UN7','UN14N','VT9','MA1','AF10','AF11'],
                    'm3':['SB2U','WB4','AF6','AF8','AF9','SB1','WB18','MT1','WB16','Mt9','MT11','MA1'],
                    'm4':['MT11','UN7','UN14','MA1','AF10','AF11','SB2','WB4','AF6','AF8','AF9','SB1'],
                    'm5':['MA1','AF10','VT9','UN14','MT9','MT11','UN7','CM3','CM8','WB6A','WB16','CP2'],
}







# features_data2 rappresenta diverse combinazioni di item (m2...m*) che si possono ottenere sul dataset risultato
# dal processing del dtaset data2 basato sul questionario mn.

# data2 = {'mn': ['MWB6A','MWB16','MMT9','MMT11','MCM3','MCM8','MVT9','MMA1','MAF10','MAF11','MSB2U','MLS2','MWB4','MAF6','MAF8','MAF9','MSB1','MWB18','MMT1','MMT5']}

features_data2 = {
                    'm2':['MWB4','MWB6A','MWB18','MMT1','MMT9','MMT11','MCM8','MVT9','MAF6','MAF8','MAF9','MAF10']
                    #,'MAF11','MSB1','MSB2U'
}



# questo dataset di fatto è uguale al precedente ad eccezione dell'item MLS2 che presenta 2 livelli invece di 10
features_data3 = {
                    'm1':['MWB4','MWB6A','MWB18','MMT1','MMT9','MMT11','MCM8','MVT9','MAF6','MAF8','MAF9','MAF10'],
                    'm2':['MWB4','MWB18','MMT1','MMT9','MMT11','MVT9','MAF6','MAF9','MAF10','MAF11','MSB1','MSB2U']
}